console.log('Hello world')

//LOOPS

//WHILE LOOP
/*
Syntax:
	while(expression/condition){
	statement
	}

*/

//Create a function that called displayMsgToSelf()
	//display a message to your past self in your console 10 times
	//Invoke the function 10 times

function displayMsgToSelf(){
	console.log("Dont text her back")
}

displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()
displayMsgToSelf()


//While loop
//while loop allow us to repeat an action or an instruction as long as the condition is true.

let count = 10;

while (count !== 0){
	console.log("Dont text her back.")
	count--;
}
/*
1st loop - Count 10
2nd - 9
3rd - 8........

9th loop - Count 2
10th loop - Count 1

If there is no decrementation, the condition is always true, thus an infinite loop.

Infinite loops will run you code block Forever until you stop it.

*/

let count1 = 5

while(count1 !== 0){
	console.log(count1)
	count1--;
}

//Mini act
//The ff while loop should display the numbers from 1 to

let count2 = 1;
while(count2 <= 5){
	console.log(count2)
	count2++;
}


//Do-While loop

//A do-while loop is like a while loop. But do-while loops guarantee that the code will be executed atleast once
/*
Syntax:
	do{
	statement
	}while(exprssion/condition)

*/

// let doWhileCounter = 1

// do{
// 	console.log(doWhileCounter)
// 	doWhileCounter++;
// }while(doWhileCounter <= 20)


// let number = Number(prompt('Give me a Number'))
// do{
// 	console.log("Do While: " + number)
// 	number+=1

// }while(number < 10)


//FOR LOOP
/*
it consists of three parts:
1. initialization - value that will track the progression of the loop
2. expression/condition that will be evaluated which will determine whether the loop will run one more time
3. finalExpression indicates how to advance the loop(++, --)

syntax:
	for(initialization; expression/condition; finalExpression)
		statement
*/

//Loop 0-20
for (let count = 0; count <= 20; count++){
	console.log(count)
}

//For loops accessing array items
let fruits = ["apple", "durian", "kiwi", "pineapple", "mango"];

//.legth property is also a property of an array. that shows the total number of items in an array.
console.log(fruits[2])
console.log(fruits.length)//5 total numbers

//A more reliable way of checking the last item is an array:
//arrayName[arrayName.lenth-1]
console.log(fruits[fruits.length-1])

//show all the items in the array in the console using loops

for(let index = 0; index < fruits.length; index++){
	console.log(fruits[index])
}

//mini-act

let country = ["RUSSIA", "GERMANY", "UKRAINE", "MALAYSIA", "CHINA", "JAPAN"];

for(let index = 0; index < country.length; index++){
	console.log(country[index-3])
}

//for loops accessing elements of a string
let myString = 'alex';
//.length it is also a property used in strings
console.log(myString.length) //4

//individual characters of a string can be access using its index number. and it is also start with 0 - nth number
console.log(myString[0])//a

//will create a loop that will printout the individual letters of the myString variable

for (let x = 0; x<myString.length; x++){
	console.log(myString[x])
}

let myName = "Jane";
/*
Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel

*/

for (let i = 0; i<myName.length; i++){
	//if the character of a name is vowel letter, display number '3'
	// console.log(myName[i])
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		) {
		console.log(3)
}	else{
	//print in all the non-vowel in the console
	console.log(myName[i])
}
}


//CONTINUE and BREAK STATEMENTS

//continue alows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

//break statements is used to terminate the current loop once a match has been found
 for(let count =0; count<= 20; count++){ 	
 	//if remainder is equal to 0, we will used the continue statement
 	if(count % 2 === 0){
 		
 		//tells the code to continue to the next iteration
 		//This ignores all statements located after the continue statements;
 		continue;
 	}
 	//The current value of a number is printed out if the remainder
 	console.log("Continue and Break: " + count)


 	//if the current value of count is grether than 10, the used the break
 	if(count > 10){
 		break;
 	}
 }


 //create a loop that will iterate based on the length of the string

 let name = "alexandro"
 for (let i = 0; i< name.length; i++){
 	console.log(name[i])	

 	//if the vowel is equal to a, continue to the next iteration
 	if (name[i].toLowerCase() === 'a'){
 		console.log("Continue to nest iteration")
 		continue;
 	}
 	if(name[i] == 'd'){
 		break;
 	}
 }

 // //for in
 // let colors = ["red", "pink", "yellow"];

 // for(let x in colors){
 	
 // }